#ifndef RECTANGLE_TyJCxfgTFX
#define RECTANGLE_TyJCxfgTFX

#include "Shape.h"


class Rectangle :
  public Shape
{
public:
  Rectangle(rectangle_t passedRect);
  double getArea() const;
  rectangle_t getFrameRect() const;

protected:
  double width_,
    height_;
};


#endif