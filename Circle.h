#ifndef CIRCLE_svHgTJVwqG
#define CIRCLE_svHgTJVwqG

#include "Shape.h"


class Circle :
  public Shape
{
public:
  Circle(point_t passedPos, double passedRadius);
  double getArea() const;
  rectangle_t getFrameRect() const;

protected:
  double rad_;
};


#endif