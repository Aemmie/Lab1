#ifndef SHAPE_zJLFzRoaRA
#define SHAPE_zJLFzRoaRA

#include "Base-types.h"


class Shape
{
public:
  virtual double getArea() const = 0;
  virtual rectangle_t getFrameRect() const = 0;
  void move(point_t point);
  void move(double x, double y);

protected:
  point_t pos_;
};


#endif